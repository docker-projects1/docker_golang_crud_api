FROM golang:1.15-alpine
MAINTAINER GINU MATHEW
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN apk add --no-cache git
Run go get github.com/go-sql-driver/mysql
RUN go build -o main . 
CMD ["/app/main"]

